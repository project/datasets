<?php

namespace Drupal\datasets\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * Define datasets entity type.
 *
 * @ConfigEntityType(
 *   id = "datasets_type",
 *   bundle_of = "dataset",
 *   label = @Translation("Datasets Type"),
 *   admin_permission = "administer datasets type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_prefix = "type",
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\datasets\Form\DatasetsTypeForm",
 *       "edit" = "\Drupal\datasets\Form\DatasetsTypeForm",
 *       "default" = "\Drupal\datasets\Form\DatasetsTypeForm",
 *       "delete" = "\Drupal\datasets\Form\DatasetsTypeDeleteForm"
 *     },
 *     "list_builder" = "\Drupal\datasets\Controller\DatasetsTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "\Drupal\datasets\Entity\Routing\DatasetsDefaultHtmlRouteProvider"
 *     }
 *   },
 *   links = {
 *     "collection" = "/admin/structure/datasets-type",
 *     "add-form" = "/admin/structure/datasets-type/add",
 *     "canonical" = "/admin/structure/datasets-type/{datasets_type}",
 *     "edit-form" = "/admin/structure/datasets-type/{datasets_type}/edit",
 *     "delete-form" = "/admin/structure/datasets-type/{datasets_type}/delete"
 *   }
 * )
 */
class DatasetsEntityType extends ConfigEntityBundleBase {

  /**
   * The dataset type machine name.
   *
   * @var string
   */
  protected $id;

  /**
   * The dataset type label name.
   *
   * @var string
   */
  protected $label;
}

<?php

namespace Drupal\datasets_metric\Views;

use Drupal\views\EntityViewsData;

/**
 * Define datasets metric views default data.
 */
class DatasetsMetricViewsDataDefault extends EntityViewsData {

  /**
   * {@inheritDoc}
   */
  public function getViewsData() {
    $view_data = parent::getViewsData();

    $view_data['dataset_metric']['reverse__dataset_metric__id']['relationship'] = $this
      ->getEntityReverseRelationship('dataset_indicator', 'metric');

    return $view_data;
  }

  /**
   * Gets an entity_reverse relationship plugin definition to the base entity.
   *
   * @param $referencing_entity_type_id
   *   Entity type id of the entity referencing this current entity.
   * @param $referencing_field_name
   *   The entity field name referencing this entity.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityReverseRelationship(
    $referencing_entity_type_id,
    $referencing_field_name
  ) {
    $referencing_entity_type = $this->entityTypeManager
      ->getDefinition($referencing_entity_type_id);
    $field_storage_definitions = $this->entityFieldManager
      ->getFieldStorageDefinitions($referencing_entity_type_id);
    $table_mapping = $this->entityTypeManager
      ->getStorage($referencing_entity_type_id)
      ->getTableMapping();

    return [
      'title' => $referencing_entity_type->getLabel(),
      'label' => $referencing_entity_type->getLabel(),
      'group' => $this->entityType->getLabel(),
      'help' => $this->t('Relate each @base_entity with its parent @referencing_entity.', [
        '@base_entity' => $this->entityType->getLabel(),
        '@referencing_entity' => $referencing_entity_type->getLabel(),
      ]),
      'id' => "entity_reverse",
      'base' => $referencing_entity_type->getDataTable() ?: $referencing_entity_type->getBaseTable(),
      'entity_type' => $referencing_entity_type->id(),
      'base field' => $referencing_entity_type->getKey('id'),
      'field_name' => $referencing_field_name,
      'field table' => $table_mapping->getDedicatedDataTableName($field_storage_definitions['metric']),
      'field field' => $referencing_field_name . '_target_id',
      'join_extra' => [
        0 => [
          'field' => 'deleted',
          'value' => 0,
          'numeric' => TRUE,
        ],
      ],
    ];
  }
}

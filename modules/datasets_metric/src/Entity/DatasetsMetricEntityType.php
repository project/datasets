<?php

namespace Drupal\datasets_metric\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * Define datasets metric entity type.
 *
 * @ConfigEntityType(
 *   id = "dataset_metric_type",
 *   bundle_of = "dataset_metric",
 *   label = @Translation("Dataset Metric Type"),
 *   admin_permission = "administer dataset metric type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_prefix = "type",
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\datasets_metric\Form\DatasetsMetricTypeForm",
 *       "edit" = "\Drupal\datasets_metric\Form\DatasetsMetricTypeForm",
 *       "default" = "\Drupal\datasets_metric\Form\DatasetsMetricTypeForm",
 *       "delete" = "\Drupal\datasets_metric\Form\DatasetsMetricTypeDeleteForm"
 *     },
 *     "list_builder" = "\Drupal\datasets_metric\Controller\DatasetsMetricTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "\Drupal\datasets_metric\Entity\Routing\DatasetsMetricDefaultHtmlRouteProvider"
 *     }
 *   },
 *   links = {
 *     "collection" = "/admin/structure/datasets/metric",
 *     "add-form" = "/admin/structure/datasets/metric/add",
 *     "canonical" = "/admin/structure/datasets/metric/{dataset_metric_type}",
 *     "edit-form" = "/admin/structure/datasets/metric/{dataset_metric_type}/edit",
 *     "delete-form" = "/admin/structure/datasets/metric/{dataset_metric_type}/delete"
 *   }
 * )
 */
class DatasetsMetricEntityType extends ConfigEntityBundleBase {

  /**
   * The dataset type machine name.
   *
   * @var string
   */
  protected $id;

  /**
   * The dataset type label name.
   *
   * @var string
   */
  protected $label;
}

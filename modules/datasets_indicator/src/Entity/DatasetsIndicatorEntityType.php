<?php

namespace Drupal\datasets_indicator\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;

/**
 * Define datasets entity type.
 *
 * @ConfigEntityType(
 *   id = "dataset_indicator_type",
 *   bundle_of = "dataset_indicator",
 *   label = @Translation("Dataset Indicator Type"),
 *   admin_permission = "administer dataset indicator type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_prefix = "type",
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\datasets_indicator\Form\DatasetsIndicatorTypeForm",
 *       "edit" = "\Drupal\datasets_indicator\Form\DatasetsIndicatorTypeForm",
 *       "default" = "\Drupal\datasets_indicator\Form\DatasetsIndicatorTypeForm",
 *       "delete" = "\Drupal\datasets_indicator\Form\DatasetsIndicatorTypeDeleteForm"
 *     },
 *     "list_builder" = "\Drupal\datasets_indicator\Controller\DatasetsIndicatorTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "\Drupal\datasets_indicator\Entity\Routing\DatasetsIndicatorDefaultHtmlRouteProvider"
 *     }
 *   },
 *   links = {
 *     "collection" = "/admin/structure/datasets/indicator",
 *     "add-form" = "/admin/structure/datasets/indicator/add",
 *     "canonical" = "/admin/structure/datasets/indicator/{dataset_indicator_type}",
 *     "edit-form" = "/admin/structure/datasets/indicator/{dataset_indicator_type}/edit",
 *     "delete-form" = "/admin/structure/datasets/indicator/{dataset_indicator_type}/delete"
 *   }
 * )
 */
class DatasetsIndicatorEntityType extends ConfigEntityBundleBase {

  /**
   * The dataset type machine name.
   *
   * @var string
   */
  protected $id;

  /**
   * The dataset type label name.
   *
   * @var string
   */
  protected $label;
}
